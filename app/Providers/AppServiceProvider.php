<?php

namespace App\Providers;

use Api\StarterKit\Serializer\ApiSerializer;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Interfaces\ArticleInterface;
use App\Repositories\ArticleRepository;
use App\Repositories\Interfaces\LearnThingInterface;
use App\Repositories\LearnThingRepository;
use App\Repositories\Interfaces\NoticeInterface;
use App\Repositories\NoticeRepository;
use App\Repositories\Interfaces\UserInterface;
use App\Repositories\UserRepository;
use App\Repositories\Interfaces\TechCategoryInterface;
use App\Repositories\TechCategoryRepository;
use App\Repositories\Interfaces\ConversationInterface;
use App\Repositories\ConversationRepository;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      if (config('api.transformer')) {
        $this->app['api.transformer']
          ->getAdapter()
          ->getFractal()
          ->setSerializer(new ApiSerializer);
      }
    }
}
